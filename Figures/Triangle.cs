﻿using System;

namespace Figures
{
    public sealed class Triangle : IFigure
    {
        public double Side1 { get; }
        public double Side2 { get; }
        public double Side3 { get; }

        public Triangle(double side1, double side2, double side3)
        {
            if (side1 < 0) throw new ArgumentOutOfRangeException(nameof(side1));
            if (side2 < 0) throw new ArgumentOutOfRangeException(nameof(side2));
            if (side3 < 0) throw new ArgumentOutOfRangeException(nameof(side3));
            if (side1 + side2 < side3
                || side1 + side3 < side2
                || side2 + side3 < side1)
                throw new ArgumentOutOfRangeException("Wrong sides.");

            Side1 = side1;
            Side2 = side2;
            Side3 = side3;
        }

        public double Area => this.Area();
    }
}
