﻿using System;

namespace Figures
{
    internal static class AreaCalculators
    {
        internal static double Area(this Circle c)
        {
            return Math.PI * c.Radius * c.Radius;
        }

        internal static double Area(this Triangle t)
        {
            var perimeter = (t.Side1 + t.Side2 + t.Side3) / 2d;
            return Math.Sqrt(perimeter * (perimeter - t.Side1) * (perimeter - t.Side2) * (perimeter - t.Side3));
        }
    }
}
