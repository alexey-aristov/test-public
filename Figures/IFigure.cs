﻿namespace Figures
{
    public interface IFigure
    {
        double Area { get; }
    }
}
