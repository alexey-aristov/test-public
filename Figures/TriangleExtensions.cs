﻿using System;

namespace Figures
{
    public static class TriangleExtensions
    {
        private static double tolerance = double.Epsilon * 10;
        public static bool IsRight(this Triangle t)
        {
            return Math.Abs(t.Side1 - PythagoreanHypotenuse(t.Side2, t.Side3)) < tolerance
                   || Math.Abs(t.Side2 - PythagoreanHypotenuse(t.Side1, t.Side3)) < tolerance
                   || Math.Abs(t.Side3 - PythagoreanHypotenuse(t.Side1, t.Side2)) < tolerance;
        }

        private static double PythagoreanHypotenuse(double side1, double side2)
        {
            return Math.Sqrt(Math.Pow(side1, 2) + Math.Pow(side2, 2));
        }
    }
}
