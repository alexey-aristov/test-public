﻿using System;
using Xunit;

namespace Figures.Tests
{
    public class AreaCalculatorTests
    {
        [Theory]
        [InlineData(0d, 0d)]
        [InlineData(1d, Math.PI)]
        [InlineData(5d, Math.PI * 25)]
        public void CircleAreaAreCorrect(double radius, double expectedArea)
        {
            // Arrange
            var circle = new Circle(radius);

            // Act
            var area = AreaCalculators.Area(circle);

            // Assert
            Assert.Equal(expectedArea, area);
        }


        // Todo: more cases.
        [Theory]
        [InlineData(0d, 0d, 0d, 0d)]
        [InlineData(3d, 4d, 5d, 6d)]
        public void TriangleAreaAreCorrect(
            double side1,
            double side2,
            double side3,
            double expectedArea)
        {
            // Arrange
            var triangle = new Triangle(side1, side2, side3);

            // Act
            var area = AreaCalculators.Area(triangle);

            // Assert
            Assert.Equal(expectedArea, area);
        }
    }
}
