﻿using Xunit;

namespace Figures.Tests
{
    public class TriangleExtensionsTests
    {
        [Theory]
        [InlineData(3d, 4d, 5d, true)]
        [InlineData(3d, 4d, 6d, false)]
        public void IsRightTriangleTest(double side1,
            double side2,
            double side3,
            bool expectedIsRight)
        {
            // Arrange
            var triangle = new Triangle(side1, side2, side3);

            // Act
            var isRight = triangle.IsRight();

            // Assert
            Assert.Equal(expectedIsRight, isRight);
        }
    }
}
